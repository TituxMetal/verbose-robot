<?php

namespace Core;

/**
	*	Config class
	*
	*/
class Config {

	/**
		*	$settings Store the settings
		*
		*/
	private $settings = [];

	/**
		*	$_instance Store a single instance of Config class
		*
		*/
	private static $_instance;

	/**
		*	getInstance() Get a single instance of this class
		*	@param string $file The path to the configuration file
		*
		*	@return Core\Config An instance of Config class
		*/
	public static function getInstance($file) {
		if(is_null(self::$_instance))
		{
			self::$_instance = new Config($file);
		}

			return self::$_instance;
		}

	/**
		*	__construct()
		*	@param string $file The path to the configuration file
		*	@return void
		*/
	public function __construct($file) {
		$this->settings = require $file;
	}

	/**
		*	get() Get a specific key from the configuration
		*	@param string $key The key from the configuration
		*	@return mixed string|null The value of the configuration key if exists or null
		*/
	public function get($key) {

		return (isset($this->settings[$key])) ? $this->settings[$key] : null;
	}

}