<?php

namespace Core;

use Core\Config;
use Slim\App as Slim;

/**
	*	Core that extends the Slim Micro Framework
	*
	*/
class Core extends Slim {

	/**
		*	Adding the settings for Slim
		*
		*/
	public function __construct() {
		parent::__construct();
		$config = Config::getInstance(CONFIG_FILE)->get('app.settings');
		$this->getContainer($config);
		return;
	}

}