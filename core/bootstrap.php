<?php

// Define some constants
define('ROOT', dirname(__DIR__));
define('DS', '/');
define('CONFIG_FILE', ROOT . DS . 'config/config.php');

// Require the autoloader from composer
require ROOT . '/vendor/autoload.php';

// Start a new core class
$app = new Core\Core();

// Require all the routes for this application
require ROOT . '/app/routes.php';