<?php

// Require the bootstrap file to boot the application
require dirname(__DIR__) . '/core/bootstrap.php';

// Running the application
$app->run();