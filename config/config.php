<?php

return [
	'db.options' => [
		'driver'   => 'pdo_mysql',
		'charset'  => 'utf8',
		'host'     => 'localhost',
		'port'     => '3306',
		'dbname'   => 'labo',
		'user'     => 'root',
		'password' => 'root',
	],
	'app' => [
		'settings' => [
			'displayErrorDetails' => true,
		],
	],
];